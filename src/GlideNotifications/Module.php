<?php

namespace GlideNotifications;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Mvc\ApplicationInterface;
use GlideNotifications\Entity\Notification;

class Module implements
AutoloaderProviderInterface, BootstrapListenerInterface, ConfigProviderInterface, ControllerPluginProviderInterface, ViewHelperProviderInterface {

    /**
     * {@inheritDoc}
     */
    public function onBootstrap(EventInterface $event) {
        /* @var $app \Zend\Mvc\ApplicationInterface */
        $app = $event->getTarget();

        $sm = $app->getServiceManager();
        $config = $sm->get('Config');
        // Add any extra notification types
        if (isset($config['GlideNotifications']['ExtraNotificationTypes']))
            Notification::add_types($config['GlideNotifications']['ExtraNotificationTypes']);
        
        // Add any extra hidden types
        if (isset($config['GlideNotifications']['ExtraHiddenNotificationTypes']))
            Notification::add_hidden_types($config['GlideNotifications']['ExtraHiddenNotificationTypes']);
        
        // Add any extra ignore types
        if (isset($config['GlideNotifications']['ExtraIgnoreUserSettingsTypes']))
            Notification::add_ignore_settings_types($config['GlideNotifications']['ExtraIgnoreUserSettingsTypes']);
    }

    /**
     * {@inheritDoc}
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
            ),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getControllerPluginConfig() {
        return array(
            'factories' => array(
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/../../src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}
