<?php

namespace GlideNotifications\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="notification_dispatch")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class NotificationDispatch {

    const TYPE_PUSH = 1;
    const TYPE_EMAIL = 2;
    const STATUS_QUEUE = 1;
    const STATUS_DISPATCHED = 2;
    const STATUS_ALREADY_SEEN = 3;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="GlideNotifications\Entity\Notification")
     * @ORM\JoinColumn(name="notification_id", referencedColumnName="id")
     *
     */
    protected $notification;

    /**
     * @ORM\Column(type="smallint", name="dispatch_type")
     *
     */
    protected $type;

    /**
     * @ORM\Column(type="smallint")
     *
     */
    protected $status;

    /**
     * @ORM\Column(type="datetime", name="date_dispatched", nullable=true)
     * 
     */
    protected $dateDispatched;

    /**
     * @ORM\Column(type="datetime", name="date_created")
     * 
     */
    protected $dateCreated;

    public function __construct($options = null) {
        if (!empty($options)) {
            $methods = get_class_methods($this);
            foreach ($options as $key => $value) {
                $method = 'set' . ucfirst($key);
                if (in_array($method, $methods)) {
                    $this->$method($value);
                }
            }
        }
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set id.
     *
     */
    public function setId($id) {
        $this->id = (int) $id;
        return $this;
    }

    public function getNotification() {
        return $this->notification;
    }

    public function setNotification($notification) {
        $this->notification = $notification;
        return $this;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDateCreated($date = null) {
        $this->dateCreated = new \DateTime('now');
    }

    public function getDateDispatched() {
        return $this->dateDispatched;
    }

    public function setDateDispatched($date) {
        $this->dateDispatched = $date;
    }

}
