<?php

namespace GlideNotifications\Entity;

interface UserInterface {
    public function getReceiveGeneralEmail();
    public function getReceivePush();
    public function unsubscribeGeneralEmails();
}
