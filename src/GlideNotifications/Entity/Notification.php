<?php

namespace GlideNotifications\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="notification")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class Notification {

    /**
     * 
     * @param string $name Type name
     * @return int Returns corresponding type number
     */
    public static function get_type($name) {
        return self::$types[$name];
    }

    /**
     * 
     * @param int $type
     * @return string Returns key of type
     */
    public static function get_type_string($type) {
        return array_search($type, self::$types);
    }

    /**
     * Expand on the built in notification types
     * 
     * @param array $types An associative array of type names and their corresponding number. An error is thrown if the number
     * or name is already in use
     */
    public static function add_types($types) {
        foreach ($types as $name => $num) {
            if (in_array($num, self::$types))
                throw new \Exception('Notification number already in use');
            if (in_array($name, array_keys(self::$types)))
                throw new \Exception('Notification name already in use');
        }

        foreach ($types as $name => $num) {
            self::$types[$name] = $num;
        }
    }

    // Never change the numbers of these built in types
    private static $types = array(
        'NEW_MSG_SENT' => 9,
        'YOU_JOINED' => 14,
        'EMAIL_CHANGED' => 15,
    );

    /**
     * 
     * @return array Returns hidden types array
     */
    public static function get_hidden_types() {
        $types = array();
        foreach (self::$hidden_types as $ht)
            $types[] = self::get_type($ht);
        return $types;
    }

    /**
     * Expand on the built in hidden types. Notifications passed here will not
     * be shown to the user while using the platform. For example the build in notification 'EMAIL_CHANGED'
     * is in this group - this notification triggers an email to the user but does not need to show
     * them anything in the platform
     * 
     * @param array $types An array of type names. An error is thrown if the name is already in recoreded as hidden.
     */
    public static function add_hidden_types($types) {
        foreach ($types as $type) {
            if (in_array($type, self::$hidden_types))
                throw new \Exception('Notification name already in hidden types');
            if (in_array($type, array_keys(self::$hidden_types)) === false)
                throw new \Exception('Notification name not found');
        }

        foreach ($types as $type) {
            self::$hidden_types[] = $type;
        }
    }

    private static $hidden_types = array(
        'YOU_JOINED',
        'EMAIL_CHANGED',
    );

    /**
     * 
     * @return array Returns types where the user settings should be ignored
     */
    public static function get_ignore_settings_types() {
        $types = array();
        foreach (self::$hidden_types as $ht)
            $types[] = self::get_type($ht);
        return $types;
    }

    /**
     * Expand on the built in ignore settings types. Notifications passed here will not
     * check if the user has enabled email notifications. For example the build in notification 'EMAIL_CHANGED'
     * is in this group - this notification triggers an email. If the user has disabled receiving email notifications
     * they still need to receive this notification
     * 
     * @param array $types An array of type names. An error is thrown if the name is already in recoreded as hidden.
     */
    public static function add_ignore_settings_types($types) {
        foreach ($types as $type) {
            if (in_array($type, self::$ignore_settings_types))
                throw new \Exception('Notification name already in ignore types');
            if (in_array($type, array_keys(self::$ignore_settings_types)) === false)
                throw new \Exception('Notification name not found');
        }

        foreach ($types as $type) {
            self::$ignore_settings_types[] = $type;
        }
    }

    private static $ignore_settings_types = array(
        'YOU_JOINED',
        'EMAIL_CHANGED',
    );

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="GlideNotifications\Entity\UserInterface")
     * @ORM\JoinColumn(name="user_from", referencedColumnName="id")
     *
     */
    protected $userFrom;

    /**
     * @ORM\ManyToOne(targetEntity="GlideNotifications\Entity\UserInterface")
     * @ORM\JoinColumn(name="user_to", referencedColumnName="id")
     *
     */
    protected $userTo;

    /**
     * @ORM\Column(type="smallint")
     *
     */
    protected $type;

    /**
     * @ORM\Column(type="smallint")
     *
     */
    protected $seen;

    /**
     * @ORM\Column(type="smallint", name="is_read")
     *
     */
    protected $read;

    /**
     * @ORM\OneToOne(targetEntity="GlideNotifications\Entity\Message")
     * @ORM\JoinColumn(name="message", referencedColumnName="id")
     *
     */
    protected $message;

    /**
     * @ORM\ManyToOne(targetEntity="GlideNotifications\Entity\NoteItem1Interface")
     * @ORM\JoinColumn(name="note_item1_id", referencedColumnName="id")
     *
     */
    protected $noteItem1;

    /**
     * @ORM\ManyToOne(targetEntity="GlideNotifications\Entity\NoteItem2Interface")
     * @ORM\JoinColumn(name="note_item2_id", referencedColumnName="id")
     *
     */
    protected $noteItem2;

    /**
     * @ORM\ManyToOne(targetEntity="GlideNotifications\Entity\NoteItem3Interface")
     * @ORM\JoinColumn(name="note_item3_id", referencedColumnName="id")
     *
     */
    protected $noteItem3;

    /**
     * @ORM\Column(type="smallint", options={"default"=0})
     * 
     */
    protected $deletedButNotRemoved = 0;

    /**
     * @ORM\Column(type="datetime", name="date_created")
     * 
     */
    protected $dateCreated;

    public function __construct($options = null) {
        if (!empty($options)) {
            $methods = get_class_methods($this);
            foreach ($options as $key => $value) {
                $method = 'set' . ucfirst($key);
                if (in_array($method, $methods)) {
                    $this->$method($value);
                }
            }
        }
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set id.
     *
     */
    public function setId($id) {
        $this->id = (int) $id;
        return $this;
    }

    public function getUserFrom() {
        return $this->userFrom;
    }

    public function setUserFrom($user) {
        $this->userFrom = $user;
        return $this;
    }

    public function getUserTo() {
        return $this->userTo;
    }

    public function setUserTo($user) {
        $this->userTo = $user;
        return $this;
    }

    public function getTypeName() {
        return array_search($this->type, self::$types);
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function getSeen() {
        return $this->seen;
    }

    public function setSeen($seen) {
        $this->seen = $seen;
        return $this;
    }

    public function getRead() {
        return $this->read;
    }

    public function setRead($read) {
        $this->read = $read;
        return $this;
    }

    public function getMessage() {
        return $this->message;
    }

    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    public function getNoteItem1() {
        return $this->noteItem1;
    }

    public function setNoteItem1($noteItem1) {
        $this->noteItem1 = $noteItem1;
        return $this;
    }

    public function getNoteItem2() {
        return $this->noteItem2;
    }

    public function setNoteItem2($noteItem2) {
        $this->noteItem2 = $noteItem2;
        return $this;
    }

    public function getNoteItem3() {
        return $this->noteItem3;
    }

    public function setNoteItem3($noteItem3) {
        $this->noteItem3 = $noteItem3;
        return $this;
    }

    public function getDeletedButNotRemoved() {
        return $this->deletedButNotRemoved;
    }

    public function setDeletedButNotRemoved($deletedButNotRemoved) {
        $this->deletedButNotRemoved = $deletedButNotRemoved;
        return $this;
    }

    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDateCreated($date = null) {
        $this->dateCreated = new \DateTime('now');
    }

    public function updateDateCreated($date) {
        $this->dateCreated = $date;
    }

}
