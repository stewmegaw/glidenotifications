<?php

namespace GlideNotifications\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="message")
 * @ORM\HasLifecycleCallbacks()
 * 
 */
class Message {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="GlideNotifications\Entity\UserInterface")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     *
     */
    protected $owner;

    /**
     * @ORM\ManyToOne(targetEntity="GlideNotifications\Entity\UserInterface")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     *
     */
    protected $sender;

    /**
     * @ORM\ManyToOne(targetEntity="GlideNotifications\Entity\UserInterface")
     * @ORM\JoinColumn(name="recipient_id", referencedColumnName="id")
     *
     */
    protected $recipient;

    /**
     * @ORM\ManyToOne(targetEntity="GlideNotifications\Entity\NoteItem1Interface", inversedBy="message")
     * @ORM\JoinColumn(name="extra_item_id", referencedColumnName="id", nullable=true)
     *
     */
    protected $extraItem;

    /**
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @ORM\Column(type="smallint", name="is_read")
     *
     */
    protected $read;

    /**
     * @ORM\Column(type="datetime", name="date_created")
     * 
     */
    protected $dateCreated;

    public function __construct($options = null) {
        if (!empty($options)) {
            $methods = get_class_methods($this);
            foreach ($options as $key => $value) {
                $method = 'set' . ucfirst($key);
                if (in_array($method, $methods)) {
                    $this->$method($value);
                }
            }
        }
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set id.
     *
     */
    public function setId($id) {
        $this->id = (int) $id;
        return $this;
    }

    public function getOwner() {
        return $this->owner;
    }

    public function setOwner($owner) {
        $this->owner = $owner;
        return $this;
    }

    public function getSender() {
        return $this->sender;
    }

    public function setSender($sender) {
        $this->sender = $sender;
        return $this;
    }

    public function getRecipient() {
        return $this->recipient;
    }

    public function setRecipient($recipient) {
        $this->recipient = $recipient;
        return $this;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = $content;
        return $this;
    }

    public function getRead() {
        return $this->read;
    }

    public function setRead($read) {
        $this->read = $read;
        return $this;
    }

    public function getExtraItem() {
        return $this->extraItem;
    }

    public function setExtraItem($extraItem) {
        $this->extraItem = $extraItem;
        return $this;
    }

    public function getDateCreated() {
        return $this->dateCreated;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDateCreated() {
        $this->dateCreated = new \DateTime('now');
    }

}
