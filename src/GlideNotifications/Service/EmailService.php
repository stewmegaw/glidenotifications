<?php

namespace GlideNotifications\Service;

use Doctrine\ORM\EntityManager;
use SlmMail\Mail\Transport\HttpTransport;

class EmailService {

    protected $entityManager;
    protected $emailTransport;
    protected $config;

    public function __construct(EntityManager $entityManager, HttpTransport $emailTransport, $config) {
        $this->entityManager = $entityManager;
        $this->emailTransport = $emailTransport;
        $this->config = $config;
    }

    public function send_email($recipient, $subject = null, $html = null, $text = null) {
        $parts = array();

        if (empty($subject))
            $subject = 'No subject';

        if (!empty($text)) {
            $text_part = new \Zend\Mime\Part($text);
            $text_part->type = "text/plain";
            $parts[] = $text_part;
        }

        if (!empty($html)) {
            $html_part = new \Zend\Mime\Part($html);
            $html_part->type = "text/html";
            $parts[] = $html_part;
        }

        $body = new \Zend\Mime\Message;
        $body->setParts($parts);

        $message = new \Zend\Mail\Message;
        $message->addFrom($this->config['GlideNotifications']['Email']['FromAddress'], $this->config['GlideNotifications']['Email']['FromName'])
                ->addTo($recipient)
                ->setSubject($subject);
        $message->addReplyTo($this->config['GlideNotifications']['Email']['ReplyToAddress'], $this->config['GlideNotifications']['Email']['ReplyToName']);
        $message->setEncoding("UTF-8");
        $message->setBody($body);

        try {
            $this->emailTransport->send($message);
        } catch (\Exception $ex) {
            \error_log($ex->getMessage());
        }
    }

    // TODO - upgrade encryption/decryption using info from link below
    // TODO - Keep DRY with invitation code
    public function add_key($html, $replace, $email, $type) {
        $key = $email . '|' . $type . '|' . time();

        // http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $this->config['GlideNotifications']['Email']['UnsubscribeSalt'], utf8_encode($key), MCRYPT_MODE_ECB, $iv);
        $encrypted_string = rtrim($encrypted_string, "\0\4");     // trim ONLY the nulls and EOTs at the END
        $encrypted_string = base64_encode($encrypted_string);
        $encrypted_string = str_replace(array('/', '+'), array('_45', '_46'), $encrypted_string);

        // The replacement might be url encoded so pass both in to be safe
        $replace = array($replace, urlencode($replace));

        return str_replace($replace, $encrypted_string, $html);
    }

    // TODO - Keep DRY with invitation code
    public function decryt_key($key) {
        // http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string

        $key = str_replace(array('_45', '_46'), array('/', '+'), $key);
        $key = base64_decode($key);
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $this->config['GlideNotifications']['Email']['UnsubscribeSalt'], $key, MCRYPT_MODE_ECB, $iv);

        if (!empty($decrypted_string)) {
            $parts = explode('|', $decrypted_string);
            if (count($parts) != 3)
                return null;
            $user = $this->entityManager->getRepository('User\Entity\User')->findOneBy(array('email' => $parts[0]));
            $type = $parts[1];

            if (empty($user))
                return null;

            return array('user' => $user, 'type' => $type);
        } else
            return null;
    }

    public function contact_us($data) {
        $this->send_email(
                $this->config['GlideNotifications']['Email']['ReplyToAddress'], $data->subject, null, $data->name . "\r\n\r\n" . $data->email . "\r\n\r\n\r\n" . $data->message
        );
    }

}
