<?php

namespace GlideNotifications\Service;

use GlideNotifications\Entity\Notification;
use Doctrine\ORM\EntityManager;

class NotificationAccessService {

    protected $entityManager;
    protected $notificationRepo;
    protected $user;

    public function __construct(EntityManager $entityManager, $authenticateService) {
        $this->entityManager = $entityManager;
        $this->notificationRepo = $this->entityManager->getRepository('GlideNotifications\Entity\Notification');
        $this->user = $authenticateService->getIdentity();
    }

    public function get($new = true, $count = 11, $force_type = null, $exclude_type_new_msg = false) {

        $query = $this->entityManager->createQuery("
            SELECT n
            FROM GlideNotifications\Entity\Notification n
            WHERE n.userTo = :userTo
            " . ($new ? ' AND n.read = 0 AND n.seen = 0' : '') . "
            " . (!empty($force_type) ? ' AND n.type = ' . $force_type : '') . "
            " . ($exclude_type_new_msg ? ' AND n.type <> ' . Notification::get_type('NEW_MSG_SENT') : '') . "
            AND n.deletedButNotRemoved <> 1
            AND n.type NOT IN (:hidden_types)
            ORDER BY n.dateCreated DESC");

        $query->setParameter('hidden_types', Notification::get_hidden_types());
        $query->setParameter('userTo', $this->user);
        $query->setMaxResults($count);
        return $query->getResult();
    }

}
