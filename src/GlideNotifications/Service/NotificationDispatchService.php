<?php

namespace GlideNotifications\Service;

use GlideNotifications\Entity\Notification;
use GlideNotifications\Entity\NotificationDispatch;
use Doctrine\ORM\EntityManager;

class NotificationDispatchService {

    private $entityManager;
    private $config;

    public function __construct(EntityManager $entityManager, $config) {
        $this->entityManager = $entityManager;
        $this->config = $config;
    }

    /**
     * Queue a general email notification
     * 
     * @param GlideNotifications\Entity\Notification $notification
     */
    public function general_email_dispatch(Notification $notification) {
        if (in_array($notification->getTypeName(), $this->config['GlideNotifications']['Email']['generalEmails'])) {

            $check_if_receiving = true;
            if (in_array($notification->getType(), Notification::get_ignore_settings_types()))
                $check_if_receiving = false;


            // Check if user has enabled email notifications
            if ($check_if_receiving && !$notification->getUserTo()->getReceiveGeneralEmail())
                return;

            $this->entityManager->persist(new NotificationDispatch(array(
                'notification' => $notification,
                'type' => NotificationDispatch::TYPE_EMAIL,
                'status' => NotificationDispatch::STATUS_QUEUE
            )));
        }
    }

    /**
     * Queue a push notification
     * 
     * @param GlideNotifications\Entity\Notification $notification
     */
    public function push_dispatch($notification) {
        return;
        // TODO - also check they are able to receive notes
        if ($notification->getUserTo()->getReceivePush()) {
            $this->entityManager->persist(new NotificationDispatch(array(
                'notification' => $notification,
                'type' => NotificationDispatch::TYPE_PUSH,
                'status' => NotificationDispatch::STATUS_QUEUE
            )));
        }
    }

    /**
     * Marks any queued dispatches as seen. This will stop the user
     * receiving an external notification if they have already seen it
     * 
     * @param GlideNotifications\Entity\Notification $notification
     */
    public function seen($notification) {
        $queued_noted = $this->entityManager->getRepository('GlideNotifications\Entity\NotificationDispatch')
                ->findBy(array('notification' => $notification, 'status' => NotificationDispatch::STATUS_QUEUE));
        foreach ($queued_noted as $qn) {
            $qn->setStatus(NotificationDispatch::STATUS_ALREADY_SEEN);
            $this->entityManager->persist($qn);
        }
    }

    /**
     * Removes a dispatch and notification. The removal is done here since
     * they normally exists together and should therefore be removed together.
     * If the dispatch has aleardy been sent then we do not delete the notification
     * and instead flag it as deleted
     * 
     * @param GlideNotifications\Entity\Notification $notification
     */
    public function remove_notification($notification) {
        if (empty($notification))
            return;

        $dispatch = $this->entityManager->getRepository('GlideNotifications\Entity\NotificationDispatch')
                ->findOneBy(array('notification' => $notification));

        if (empty($dispatch)) {
            // No dispatch exists so just remove notification
            $this->entityManager->remove($notification);
        } else {
            if ($dispatch->getStatus() == NotificationDispatch::STATUS_DISPATCHED) {
                // Dispatch already sent. We dont want to lose this info so flag the notifiation
                // as 'deleted but not removed'
                $notification->setDeletedButNotRemoved(1);
                $this->entityManager->persist($notification);
            } else {
                // Dispatch not sent. Remove it and the notification.
                $this->entityManager->remove($notification);
                $this->entityManager->remove($dispatch);
            }
        }
    }

}
