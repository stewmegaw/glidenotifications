<?php

namespace GlideNotifications\Service;

use GlideNotifications\Entity\Message;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use GlideNotifications\Service\NotificationCRUDService;

class MessageService {

    public $entityManager;
    public $messageRepo;
    public $notificationCRUDService;
    public $user;

    public function __construct(EntityManager $entityManager, $authenticateService, NotificationCRUDService $notificationCRUDService) {
        $this->entityManager = $entityManager;
        $this->messageRepo = $this->entityManager->getRepository('GlideNotifications\Entity\Message');
        $this->user = $authenticateService->getIdentity();
        $this->notificationCRUDService = $notificationCRUDService;
    }

    public function get_conversation($user) {
        $query = $this->entityManager->createQuery(
                "SELECT m
                FROM GlideNotifications\Entity\Message m
                WHERE m.owner = :owner AND
                ((m.sender = :sender1 AND m.recipient = :recip2)
                OR (m.sender = :sender2 AND m.recipient = :recip1))
                ORDER BY m.dateCreated DESC");

        $query->setParameter('owner', $this->user);
        $query->setParameter('sender1', $this->user);
        $query->setParameter('sender2', $user);
        $query->setParameter('recip1', $this->user);
        $query->setParameter('recip2', $user);
        $messages = $query->getResult();

        return $messages;
    }

    public function new_message($data) {
        $recipient = $this->entityManager->getRepository('User\Entity\User')->find($data->{recipient});
        $message = new Message(array(
            'sender' => $this->user,
            'content' => $data->{content},
            'recipient' => $recipient
        ));

        $message2 = $this->insert($message, $recipient);

        $this->notificationCRUDService->new_message($message2);

        $this->entityManager->flush();

        return true;
    }

    public function insert($message1, $recipient) {


        $message2 = clone $message1;
        $message1->setOwner($this->user);
        $message1->setRead(1);

        $message2->setOwner($recipient);
        $message2->setRead(0);

        $this->entityManager->persist($message1);
        $this->entityManager->persist($message2);

        return $message2;
    }

    public function get_msg($id) {
        return $this->messageRepo->find($id);
    }

    public function msg_read($id) {
        $message = $this->get_msg($id);
        $query = $this->entityManager->createQuery("
                UPDATE GlideNotifications\Entity\Message m 
                SET m.read = ?1
                WHERE m.dateCreated <= :date AND m.sender = :sender AND m.recipient = :recip
            ");

        $query->setParameter(1, 1);
        $query->setParameter('date', $message->getDateCreated());
        $query->setParameter('sender', $message->getSender());
        $query->setParameter('recip', $this->user);
        $query->getResult();

        return true;
    }

}
