<?php

namespace GlideNotifications\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MessageServiceFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {

        //dependencies
        $entityManager = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $config = $serviceLocator->get('Config');
        $authenticateService = $serviceLocator->get($config['GlideNotifications']['UserAuthenticationService']);
        $notificationCRUDService = $serviceLocator->get('GlideNotifications\Service\NotificationCRUD');

        //dependency injections
        return new MessageService($entityManager, $authenticateService, $notificationCRUDService);
    }

}
