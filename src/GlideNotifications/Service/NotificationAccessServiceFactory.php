<?php

namespace GlideNotifications\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class NotificationAccessServiceFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {

        //dependencies
        $entityManager = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $config = $serviceLocator->get('Config');
        $authenticateService = $serviceLocator->get($config['GlideNotifications']['UserAuthenticationService']);

        //dependency injections
        return new NotificationAccessService($entityManager, $authenticateService);
    }

}
