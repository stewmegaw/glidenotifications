<?php

namespace GlideNotifications\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class NotificationCRUDServiceFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        //dependencies
        $entityManager = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $config = $serviceLocator->get('Config');
        $authenticateService = $serviceLocator->get($config['GlideNotifications']['UserAuthenticationService']);
        $notificationDispatchService = $serviceLocator->get('GlideNotifications\Service\NotificationDispatch');

        //dependency injections
        return new NotificationCRUDService($entityManager, $authenticateService, $notificationDispatchService, $config);
    }

}
