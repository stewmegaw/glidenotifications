<?php

namespace GlideNotifications\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EmailServiceFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {

        //dependencies
        $entityManager = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $emailTransport = $serviceLocator->get('SlmMail\Mail\Transport\SendGridTransport');
        $config = $serviceLocator->get('Config');

        //dependency injections
        return new EmailService($entityManager, $emailTransport, $config);
    }

}
