<?php

namespace GlideNotifications\Service;

use GlideNotifications\Entity\Notification;
use Doctrine\ORM\EntityManager;
use GlideNotifications\Service\NotificationDispatchService;

class NotificationCRUDService {

    public $entityManager;
    public $notificationRepo;
    public $notificationDispatchService;
    public $user;
    public $config;

    public function __construct(EntityManager $entityManager, $authenticateService, NotificationDispatchService $notificationDispatchService, $config) {
        $this->entityManager = $entityManager;
        $this->notificationRepo = $this->entityManager->getRepository('GlideNotifications\Entity\Notification');
        $this->user = $authenticateService->getIdentity();
        $this->notificationDispatchService = $notificationDispatchService;
        $this->config = $config;
    }

    /**
     * Sets one or more notification 'seen' flags to true
     * 
     * @param array $notes Notifications just delivered to the users view. We can set the 'seen' flag
     * for each notification in this array
     * @return array $notes The original array with updated seen flag for each notification
     */
    public function seen($notes) {
        foreach ($notes as $idx => $n) {
            $query = $this->entityManager->createQuery("
            UPDATE GlideNotifications\Entity\Notification n
            SET n.seen = 1
            WHERE n.userTo = :userTo
            AND n.id = :id");
            $query->setParameter('userTo', $this->user);
            $query->setParameter('id', $n['id']);
            $query->getResult();
            $this->notificationDispatchService->seen($this->notificationRepo->find($n['id']));
            $notes[$idx]['seen'] = 1;
        }
        return $notes;
    }

    public function seen_inbox($inbox) {
        $messageRepo = $this->entityManager->getRepository('GlideNotifications\Entity\Message');
        foreach ($inbox as $m) {
            $message = $messageRepo->find($m['id']);
            $query = $this->entityManager->createQuery("
            UPDATE GlideNotifications\Entity\Notification n
            SET n.seen = 1
            WHERE n.userTo = :userTo
            AND n.message = :message");
            $query->setParameter('userTo', $this->user);
            $query->setParameter('message', $message);
            $query->getResult();
            $this->notificationDispatchService->seen($this->notificationRepo->findBy(array('userTo' => $this->user, 'message' => $message)));
        }
    }

    public function read($id) {
        $query = $this->entityManager->createQuery("
            UPDATE GlideNotifications\Entity\Notification n
            SET n.seen = 1, n.read = 1
            WHERE n.id = :id");
        $query->setParameter('id', $id);
        $query->getResult();
        $this->notificationDispatchService->seen($this->notificationRepo->find($id));
        return true;
    }

    public function new_message($message) {
        $query = $this->entityManager->createQuery("
            SELECT n
            FROM GlideNotifications\Entity\Notification n
            JOIN n.message m
            WHERE n.message IS NOT NULL
            AND m.sender = :sender
            AND m.recipient = :recipient");
        $query->setParameter('sender', $message->getSender());
        $query->setParameter('recipient', $message->getRecipient());
        $old_msg_notes = $query->getResult();
        foreach ($old_msg_notes as $omn)
            $this->notificationDispatchService->remove_notification($omn);

        $notification = new Notification(array(
            'type' => Notification::get_type('NEW_MSG_SENT'),
            'message' => $message,
            'userFrom' => $message->getSender(),
            'userTo' => $message->getRecipient(),
            'read' => 0,
            'seen' => 0,
        ));
        $this->entityManager->persist($notification);

        $this->notificationDispatchService->general_email_dispatch($notification);
        $this->notificationDispatchService->push_dispatch($notification);
    }

    public function new_registration() {
        $notification = new Notification(array(
            'userFrom' => $this->entityManager->getRepository($this->config['GlideNotifications']['UserEntityName'])->find($this->config['GlideNotifications']['Provider_user_id']),
            'userTo' => $this->user,
            'type' => Notification::get_type('YOU_JOINED'),
            'read' => 1,
            'seen' => 1
        ));
        $this->entityManager->persist($notification);
        $this->notificationDispatchService->general_email_dispatch($notification);
    }

    public function email_changed() {
        $note4removal = $this->notificationRepo->findOneBy(array(
            'type' => Notification::get_type('EMAIL_CHANGED'),
            'userTo' => $this->user
        ));
        $this->notificationDispatchService->remove_notification($note4removal);

        $notification = new Notification(array(
            'userFrom' => $this->entityManager->getRepository($this->config['GlideNotifications']['UserEntityName'])->find($this->config['GlideNotifications']['Provider_user_id']),
            'userTo' => $this->user,
            'type' => Notification::get_type('EMAIL_CHANGED'),
            'read' => 1,
            'seen' => 1
        ));
        $this->entityManager->persist($notification);
        $this->notificationDispatchService->general_email_dispatch($notification);
    }

    public function just_served_msgs($user) {
        $query = $this->entityManager->createQuery("
            SELECT n
            FROM GlideNotifications\Entity\Notification n
            WHERE n.read = 0
            AND n.userTo = :userTo
            AND n.userFrom = :userFrom
            AND n.message IS NOT NULL");
        $query->setParameter('userTo', $this->user);
        $query->setParameter('userFrom', $user);
        $results = $query->getResult();
        foreach ($results as $r) {
            $r->setRead(1);
            $r->setSeen(1);
            $this->entityManager->persist($r);
            $this->notificationDispatchService->seen($r);
        }
        $this->entityManager->flush();

        $query = $this->entityManager->createQuery("
            UPDATE GlideNotifications\Entity\Message m
            SET m.read = 1
            WHERE m.owner = :owner
            AND m.sender = :sender
            AND m.recipient = :recipient");
        $query->setParameter('owner', $this->user);
        $query->setParameter('recipient', $this->user);
        $query->setParameter('sender', $user);
        $query->getResult();
        return true;
    }

}
