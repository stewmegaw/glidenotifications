<?php

namespace GlideNotifications\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EmailControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $serviceLocator = $serviceLocator->getServiceLocator();
        $entityManager = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $notificationDispatchService = $serviceLocator->get('GlideNotifications\Service\NotificationDispatch');
        $emailService = $serviceLocator->get('GlideNotifications\Service\Email');
        $config = $serviceLocator->get('Config');

        //dependency injections
        $controller = new EmailController($entityManager, $notificationDispatchService, $emailService, $config, $serviceLocator);
        return $controller;
    }

}
