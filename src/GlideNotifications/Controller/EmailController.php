<?php

namespace GlideNotifications\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Console\Request as ConsoleRequest;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use GlideNotifications\Service\NotificationDispatchService;
use GlideNotifications\Entity\Notification;
use GlideNotifications\Entity\NotificationDispatch;
use Doctrine\ORM\EntityManager;
use GlideNotifications\Service\EmailService;

class EmailController extends AbstractActionController {

    private $notificationDispatchService;
    private $entityManager;
    private $dispatchRepo;
    private $emailService;
    private $config;
    private $sl;

    const TYPE_GENERAL_EMAIL = 1;
    const TYPE_WEEKLY_EMAIL = 2;
    const TYPE_CONFIRM_EMAIL = 3;
    const GENERAL_EMAIL_DELAY = 30;

    // ZF2 URL helpers dont work in console so define routes here and in constructor
    private $routes = array(
        'route_email' => '/email',
        'route_unsubscribe' => '/unsubscribe',
        'route_confirm' => '/confirm',
    );
    private $subjects;
    private $paths;

    public function __construct(EntityManager $entityManager, NotificationDispatchService $notificationDispatchService, EmailService $emailService, $config, $sl) {
        $this->entityManager = $entityManager;
        $this->notificationDispatchService = $notificationDispatchService;
        $this->emailService = $emailService;
        $this->config = $config;
        $this->sl = $sl;

        $this->routes = array_merge($this->routes, $this->config['GlideNotifications']['Email']['routes']);

        $this->subjects = array_merge($this->config['GlideNotifications']['Email']['subjects'], $this->config['GlideNotifications']['Email']['extra']['subjects']);
        $this->paths = array_merge($this->config['GlideNotifications']['Email']['templateNames'], $this->config['GlideNotifications']['Email']['extra']['templateNames']);
    }

    /**
     * Action called from console cron. Sends any type general emails
     * currently in the dispatch queue
     */
    public function generalAction() {
        try {
            $request = $this->getRequest();

            // Make sure that we are running in a console and the user has not tricked our
            // application into running this action from a public web server.
            if (!$request instanceof ConsoleRequest) {
                throw new \RuntimeException('You can only use this action from a console!');
            }

            // Get all notification that need dispatching
            $query = $this->entityManager->createQuery("
            SELECT nd
            FROM GlideNotifications\Entity\NotificationDispatch nd
            WHERE nd.type = :type
            AND nd.status = :status
            AND nd.dateCreated < :datetime");
            $query->setParameter('type', NotificationDispatch::TYPE_EMAIL);
            $query->setParameter('status', NotificationDispatch::STATUS_QUEUE);
            $datetime = new \DateTime();
            $datetime->modify('-' . self::GENERAL_EMAIL_DELAY . ' seconds');
            $query->setParameter('datetime', $datetime);
            $queue = $query->getResult();

            foreach ($queue as $dispatch) {
                $notification = $dispatch->getNotification();
                $userTo = $notification->getUserTo();

                $html = $this->build_email($notification, 'html');
                $text = $this->build_email($notification, 'text');

                // Add keys
                if (!empty($html)) {
                    if ($notification->getType() == Notification::get_type('EMAIL_CHANGED'))
                        $html = $this->emailService->add_key($html, '$confirm_key$', $userTo->getEmail(), self::TYPE_CONFIRM_EMAIL);
                    $html = $this->emailService->add_key($html, '$unsubscribe_key$', $userTo->getEmail(), self::TYPE_GENERAL_EMAIL);
                }
                if (!empty($text)) {
                    if ($notification->getType() == Notification::get_type('EMAIL_CHANGED'))
                        $text = $this->emailService->add_key($text, '$confirm_key$', $userTo->getEmail(), self::TYPE_CONFIRM_EMAIL);
                }

                // Update dispatch status
                $dispatch->setStatus(NotificationDispatch::STATUS_DISPATCHED);
                $dispatch->setDateDispatched(new \DateTime());
                $this->entityManager->persist($dispatch);

                // Send email
                $this->emailService->send_email($userTo->getEmail(), $this->subjects[$notification->getTypeName()], $html, $text);

                $this->entityManager->flush();
            }
        } catch (\Exception $ex) {
            \error_log($ex->getMessage());
            return $ex->getMessage();
        }
    }

    /**
     * Action to test html type general emails.
     * Expects parameter dispatch id to be set.
     */
    public function generalTestAction() {
        if (APPLICATION_ENV != 'development')
            exit;
        $dispatch_id = $this->params('param1', null);

        $sendRealEmail = $this->params('param2', false);

        $dispatch = $this->entityManager->getRepository('GlideNotifications\Entity\NotificationDispatch')->find($dispatch_id);

        if (empty($dispatch_id) || empty($dispatch))
            throw new \Exception('No dispatch found');
        $notification = $dispatch->getNotification();
        $userTo = $notification->getUserTo();

        $text = $this->build_email($notification, 'text');
        $html = $this->build_email($notification, 'html');
        if (empty($html))
            return $this->getResponse()->setContent('No template found');

        // Add keys
        if (!empty($html)) {
            if ($notification->getType() == Notification::get_type('EMAIL_CHANGED'))
                $html = $this->emailService->add_key($html, '$confirm_key$', $userTo->getEmail(), self::TYPE_CONFIRM_EMAIL);
            $html = $this->emailService->add_key($html, '$unsubscribe_key$', $userTo->getEmail(), self::TYPE_GENERAL_EMAIL);
        }
        if (!empty($text)) {
            if ($notification->getType() == Notification::get_type('EMAIL_CHANGED'))
                $text = $this->emailService->add_key($text, '$confirm_key$', $userTo->getEmail(), self::TYPE_CONFIRM_EMAIL);
        }

        if ($sendRealEmail === 'send') {
            $this->emailService->send_email($userTo->getEmail(), $this->subjects[$notification->getTypeName()], $html, $text);
            return $this->getResponse()->setContent('Email sent... hopefully');
        } else
            return $this->getResponse()->setContent($html);
    }

    /**
     * 
     * Action called to unsubscribe from emails
     */
    public function unsubscribeAction() {
        $key = $this->params('param1', null);

        $info = $this->emailService->decryt_key($key);

        if (empty($info) || empty($info['user']))
            $msg = 'Problem unsubscribing. Please contact ' . $this->config['GlideNotifications']['Email']['ReplyToAddress'] . ' for help';
        else {
            $user = $info['user'];

            $settings = $user->unsubscribeGeneralEmails();
            if (!empty($settings)) {
                $this->entityManager->persist($settings);
                $this->entityManager->flush();
            }

            if ($info['type'] == self::TYPE_GENERAL_EMAIL)
                $type = 'general';
            else
                $type = 'weekly';
            $msg = $user->getEmail() . ' is successfully unsubscribed from ' . $type . ' emails. You can re-subscribe by logging in and navigating to your settings area.';
        }

        return $this->redirect()->toRoute('home', array(), array('query' =>
                    array('msg' => $msg)
        ));
    }

    /**
     * 
     * Action called to confirm an email address
     */
    public function confirmAction() {
        $key = $this->params('param1', null);

        $info = $this->emailService->decryt_key($key);

        if (empty($info) || empty($info['user']))
            $msg = 'Problem confirming email. Please contact ' . $this->config['GlideNotifications']['Email']['ReplyToAddress'] . ' for help';
        else {
            $user = $info['user'];
            $confirmed = $user->getEmailConfirmed();

            if (!$confirmed) {
                $user->setEmailConfirmed(1);
                $this->entityManager->persist($user);
                $this->entityManager->flush();
                $msg = $user->getEmail() . ' is successfully confirmed';
            } else
                $msg = 'Already confirmed';
        }

        return $this->redirect()->toRoute('home', array(), array('query' =>
                    array('msg' => $msg)
        ));
    }

    private function build_email($notification, $type) {
        $viewRender = $this->getServiceLocator()->get('ViewRenderer');

        $content_path = $this->config['GlideNotifications']['Email']['templatesPath'] . $type . '/' . $this->paths[$notification->getTypeName()];

        $resolver = $this->getEvent()
                ->getApplication()
                ->getServiceManager()
                ->get('Zend\View\Resolver\TemplatePathStack');

        if (false === $resolver->resolve($content_path))
            return null;

        // Get content
        $content_view = new ViewModel(array_merge($this->routes, array('n' => $notification, 'sl' => $this->sl)));
        $content_view->setTemplate($content_path);
        $content = $viewRender->render($content_view);

        // Get content in layout
        $layout_view = new ViewModel(array_merge($this->routes, array('content' => $content)));
        $layout_view->setTemplate($this->config['GlideNotifications']['Email']['layoutsPath'] . $type . '/layout');
        $layoutAndContent = $viewRender->render($layout_view);

        if ($type == 'html') {
            $css = file_get_contents('./data/resources/mui-email-inline.css');
            $cssToInlineStyles = new CssToInlineStyles();
            $convertedHtml = $cssToInlineStyles->convert($layoutAndContent, $css);
            // Add in style tag to head. We do this after conversion as the conversion
            // breaks it.
            $style_view = new ViewModel();
            $style_view->setTemplate($this->config['GlideNotifications']['Email']['layoutsPath'] . 'html/styleTag');
            $style_tag_html = $viewRender->render($style_view);
            $idxOfHead = strpos($convertedHtml, '</head>');
            $finalHtml = substr($convertedHtml, 0, $idxOfHead) . $style_tag_html . substr($convertedHtml, $idxOfHead);

            return $finalHtml;
        }

        return $layoutAndContent;
    }

}
