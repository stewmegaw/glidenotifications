// 'Request' is a required object for communication with server

var Notifications_Ctrl = {
    new:{
        notes:[],
        msgs:[],
    },
    // Called by the notifications check loop
    new_notes: function (new_notifs) {
        var NC = Notifications_Ctrl;
        
        // Split new notifs into notes & msgs
        NC.new.notes = [];
        NC.new.msgs = [];
        for(var i = 0; i < new_notifs.length; i++)
        {
            var view_open = NC.view_open(new_notifs[i]);
            if(!view_open)
            {   
                if(new_notifs[i].type == ENUMS.NOTE_NEW_MSG_SENT)
                    NC.new.msgs.push(new_notifs[i]);
                else
                    NC.new.notes.push(new_notifs[i]);
            }
        }

        Notifications_Ctrl.last_received_items = new_notifs;

        EventEmitter.dispatch('new_notes', NC.new.notes.length);
        EventEmitter.dispatch('new_msgs', NC.new.msgs.length);
    },
    // Returns true if the user is already looking at the screen related to the notification. Else false.
    view_open: function (notification) {
            if (!Number(notification.seen))
            {
                var type = notification.type;
                switch (type)
                {
                    case ENUMS.NOTE_OFFER_HOST_MADE:
                    case ENUMS.NOTE_OFFER_HOST_UPDATED:
                    case ENUMS.NOTE_OFFER_HOST_CANCELED:
                    case ENUMS.NOTE_OFFER_ACCEPTED:
                    case ENUMS.NOTE_OFFER_REJECTED:
                    case ENUMS.NOTE_OFFER_CANCEL_ACCEPTED:
                    case ENUMS.NOTE_OFFER_TRIP_UPDATED:
                        if (EventEmitter.dispatch('trip/'+notification.offerTripId, null, true) === true)
                            return true;
                        break;
                    case ENUMS.NOTE_FRIEND_NEW_TRIP:
                    case ENUMS.NOTE_FOF_NEW_TRIP:
                        if (EventEmitter.dispatch('trip/'+notification.tripId, null, true) === true)
                            return true;
                        break;
                    case ENUMS.NOTE_NEW_MSG_SENT:
                        if (EventEmitter.dispatch('messages/'+notification.userFromId, null, true) === true)
                            return true;
                        break;
                }
        }

        return false;
    },
    // Set notification read
    read: function (notification, callback) {
        var query = Object.create(Request.make);
        query.run({
            request: 'notification/'+notification.id,
            method: "PATCH",
            done: function (r)
            {
                var NCND = Notifications_Ctrl.notifications_data;
                var notifications_data_length = NCND.length;
                for (var i = 0; i < notifications_data_length; i++)
                    if (NCND[i].id == notification.id)
                        NCND[i].read = 1;

                EventEmitter.dispatch('notes_updates', NCND);

                if (callback)
                    callback();
            },
            background_process: true
        });
    },
    // Called when a view of the notifications is opened
    get: function (props) {
        var NC = Notifications_Ctrl;
         if(NC.notifications_data.length > 0)
            props.done(NC.notifications_data);


        var g_props = {
            request:"notification",
            method: "GET",
            _done: function (r) {
                var notes = r._embedded.notification;
                if (notes.length > 0)
                {
                    NC.last_received_items = notes;
                }
                NC.notifications_data = notes;
                props.done(notes);
            }
        };
        g_props = $.extend(g_props, props);
        var query = Object.create(Request.make);
        query.run(g_props);
    },
    // These are notifications received when the user
    // accesses their notifications view
    notifications_data: [],
    // Last received can be either notifications_data
    // or more probably from the new notifications loop
    last_received_items: [],
    // Called when views are manually opened in order to set corresponding unread notifications to read 
    manually_read: function (params) {
        // Data types can be
        // 1. params : {type:'trip', id:'sfds'}
        // 2. params : {type:'msg', user_from:34}
        // 3. params : {type:'profile', user_from:34}
        // 4. params : {type:'connects', other_user:null}

        (function () {
            var last_received_items_length = Notifications_Ctrl.last_received_items.length;
            var params_length = params.length;
            for (var z = 0; z < params_length; z++)
            {
                var param = params[z];

                for (var i = 0; i < last_received_items_length; i++)
                {
                    var note = Notifications_Ctrl.last_received_items[i];
                    if (Number(note.read) == 0)
                    {
                        switch (note.type)
                        {
                            case ENUMS.NOTE_OFFER_HOST_MADE:
                            case ENUMS.NOTE_OFFER_HOST_UPDATED:
                            case ENUMS.NOTE_OFFER_HOST_CANCELED:
                            case ENUMS.NOTE_OFFER_ACCEPTED:
                            case ENUMS.NOTE_OFFER_REJECTED:
                            case ENUMS.NOTE_OFFER_CANCEL_ACCEPTED:
                            case ENUMS.NOTE_OFFER_TRIP_UPDATED:
                                if (note.offerTripId == param.id && param.type == 'trip')
                                {
                                    Notifications_Ctrl.Start();
                                    return;
                                }
                                break;
                            case ENUMS.NOTE_FRIEND_NEW_TRIP:
                            case ENUMS.NOTE_FOF_NEW_TRIP:
                                if (note.tripId == param.id && param.type == 'trip')
                                {
                                    Notifications_Ctrl.Start();
                                    return;
                                }
                                break;
                            case ENUMS.NOTE_NEW_MSG_SENT:
                                if (note.userFromId == param.id && param.type == 'messages')
                                {
                                    Notifications_Ctrl.Start();
                                    return;
                                }
                                break;
                            case ENUMS.NOTE_FRIEND_JOINED:
                                if (note.userFromId == param.id && param.type == 'profile')
                                {
                                    Notifications_Ctrl.Start();
                                    return;
                                }
                                break;
                        }
                    }
                }
            }
        })();
    }
};

// Functions related to new notifications check loop
Notifications_Ctrl.interval = 45; // 45 secs
Notifications_Ctrl.timer = null;
Notifications_Ctrl.Start = function (onlyIfNotStarted) {
    if(onlyIfNotStarted && Notifications_Ctrl.timer != null)
        return;

    Notifications_Ctrl.Clear_Timer();

    var query = Object.create(Request.make);
    query.run({
        request: 'notification?new=1',
        method:"GET",
        done: function (r) {
            Notifications_Ctrl.new_notes(r._embedded.notification);
        },
        always: function () {
            Notifications_Ctrl.timer = setTimeout(function () {
                Notifications_Ctrl.Start()
            }, 1000 * Notifications_Ctrl.interval);
        },
        background_process: true
    });
};

Notifications_Ctrl.Clear_Timer = function () {
    if (Notifications_Ctrl.timer)
    {
        clearTimeout(Notifications_Ctrl.timer);
        Notifications_Ctrl.timer = null;
    }
};