const React = require('react');
const Radium = require('radium');

import {List, ListItem} from 'material-ui/List';
import Card from 'material-ui/Card';

const NotificationsListItems = require('./notificationsListItems');
const Loading = require('../helpers/loading');
const Notifications_Ctrl = require('../Common/Notifications');

var Notifications = React.createClass({
	contextTypes: {
        baseStyles:React.PropTypes.object.isRequired,
        router:React.PropTypes.object.isRequired,
    },
    componentDidMount:function() {
    	var _this = this;
    	
		EventEmitter.dispatch('progress', 66);

    	this.get();

    	EventEmitter.subscribe('new_notes',function(o){
    		if(_this.isMounted())
	    		_this.get();
    	});

    	 EventEmitter.subscribe('notes_updates', function(n){
    	 	if(_this.isMounted())
	    	 	_this.setState({notes:n});
    	 },true);

    	 EventEmitter.subscribe('resize',function(){
			if(_this.isMounted())
			{
				var window_height = $(window).height();
				var tb_height = $('#appbar').height();
	        
		        $(".win_resizable").height(window_height - tb_height);
			}
		});
    	EventEmitter.dispatch('resize');
    },
	getInitialState() {
		return {
			notes: [],
			notes_received:0,
		}
	},
	get:function() {
		var _this = this;
		Notifications_Ctrl.get({
    		done: function(notes) {
				EventEmitter.dispatch('progress', 100);
		    	_this.setState({notes:notes,notes_received:1}); 
    		}
    	});
	},
	handleClick: function (e) {
        
    },
	render: function  () {
		var baseStyles = this.context.baseStyles;

		let styles = {
			msg_list:{
				paddingTop:0,
				paddingBottom:0
			}
		};

		return (
			<div className="win_resizable" style={baseStyles.contentWrappers.noWinScroll}>
				<div style={baseStyles.wrapper}>
					<div style={baseStyles.inner_wrapper}>
						<Card style={{margin:"26px 0"}}>
							<List style={styles.msg_list}>
							    {!this.state.notes_received ? <Loading/> :
								    <NotificationsListItems items={this.state.notes} showDate={true} itemClicked={this.handleClick} />
							    }
							</List>
						</Card>				    
					</div>
				</div>
			</div>
		);
	} 
});

module.exports = Radium(Notifications);