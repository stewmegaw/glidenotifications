const React = require('react');
const Radium = require('radium');

import {List, ListItem} from 'material-ui/List';
import ClickAwayListener from 'material-ui/internal/ClickAwayListener'


const Inbox_Ctrl = require('../Common/Inbox');
const NotificationsListItems = require('./notificationsListItems'); 
const InboxListItems = require('./inboxListItems'); 
const Loading = require('../helpers/loading');
const Notifications_Ctrl = require('../Common/Notifications');


var MessageMenu = React.createClass({
	handleClick: function (e) {
        this.props.close();
    },
	componentClickAway(e) {
		this.props.close();
	},
    componentDidMount:function() {
    	var _this = this;
    	var i_ctlr = Inbox_Ctrl.get_instance();

    	i_ctlr.get({
    		max:2,
    		done: function(msgs) {
    			_this.setState({inbox:msgs,inbox_received:1}); 
    		}
    	});
    	
    	Notifications_Ctrl.get({
    		request:"notification?msgMenu=1",
    		done: function(notes) {
		    	_this.setState({notes:notes,notes_received:1}); 
    		}
    	});

    	EventEmitter.subscribe('notes_updates', function(n){
    	 	_this.setState({notes:n});
    	 },true);
    },
	getInitialState() {
		return {
			inbox: [],
			inbox_received:0,
			notes: [],
			notes_received:0,
		}
	},
	render: function  () {

		let styles = {
			msg_list:{
				paddingTop:0,
				paddingBottom:0,
				background: '#FFFFFF'
			},
			msg_header:{
			    background: '#F1F1F1',
			    fontWeight:500
			}
		};

		return (
			<ClickAwayListener onClickAway={this.componentClickAway}>
				<div key="mmenuKey" style={this.props.style} className="material_boxShadow1">
					<List key="inboxKey" style={styles.msg_list}>
						<ListItem
						  	key={0}
						    secondaryText="Inbox"
						    innerDivStyle={{paddingTop:16}}
						    style={styles.msg_header}
						    disabled={true} />
					</List>
					{!this.state.inbox_received ? <div style={styles.msg_list}><Loading/></div> :
						<InboxListItems items={this.state.inbox} more={2} itemClicked={this.handleClick} />}
					<List key="noteKey" style={styles.msg_list}>
						<ListItem
						  	key={0}
						    secondaryText="Notifications"
						    innerDivStyle={{paddingTop:16}}
						    style={styles.msg_header}
						    disabled={true} />
					</List>
				    {!this.state.notes_received ? <div style={styles.msg_list}><Loading/></div> :
					    <NotificationsListItems items={this.state.notes} more={2} itemClicked={this.handleClick} />
				    }
				</div>
			</ClickAwayListener>
		);
	} 
});

module.exports = Radium(MessageMenu);