const React = require('react');
const Radium = require('radium');

import {List, ListItem} from 'material-ui/List';
import Card from 'material-ui/Card';

const Inbox_Ctrl = require('../Common/Inbox');
const InboxListItems = require('./inboxListItems'); 
const Loading = require('../helpers/loading');


var Inbox = React.createClass({
	contextTypes: {
        baseStyles:React.PropTypes.object.isRequired,
        router:React.PropTypes.object.isRequired,
    },
    componentDidMount:function() {
    	var _this = this;
    	var i_ctlr = Inbox_Ctrl.get_instance();

    	EventEmitter.dispatch('progress', 66);

    	function get() {
	    	i_ctlr.get({
	    		done: function(msgs) {
	    			_this.setState({inbox:msgs,inbox_received:1}); 
			    	EventEmitter.dispatch('progress', 100);
	    		}
	    	});
    	}
    	get();

    	EventEmitter.subscribe('new_msgs',function(o){
    		if(_this.isMounted())
	    		get();
    	});

    	 EventEmitter.subscribe('resize',function(){
			if(_this.isMounted())
			{
				var window_height = $(window).height();
				var tb_height = $('#appbar').height();
	        
		        $(".win_resizable").height(window_height - tb_height);
			}
		});
    	EventEmitter.dispatch('resize');
    },
	getInitialState() {
		return {
			inbox: [],
			inbox_received:0,
		}
	},
	handleClick: function (e) {
        
    },
	render: function  () {
	    var baseStyles = this.context.baseStyles;

		let styles = {
			msg_list:{
				paddingTop:0,
				paddingBottom:0
			}
		};

		return (
			<div className="win_resizable" style={baseStyles.contentWrappers.noWinScroll}>
				<div style={baseStyles.wrapper}>
					<div style={baseStyles.inner_wrapper}>
						<Card style={{margin:"26px 0"}}>
							<List key="inboxKey" style={styles.msg_list}>
							    {!this.state.inbox_received ? <Loading/> :
									<InboxListItems items={this.state.inbox} headers={true} showDate={true} itemClicked={this.handleClick} />
								}
							</List>
						</Card>
					</div>
				</div>
			</div>
		);
	} 
});

module.exports = Radium(Inbox);