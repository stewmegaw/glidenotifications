const React = require('react');

import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';

const Notifications_Ctrl = require('../Common/Notifications');
const Date_Time = require('../Common/Date_Time');

var InboxListItems = React.createClass({
	getInitialState: function(){
		return {
			items:[],
			done:0,
			new_hidden:0,
		}
	},
	contextTypes: {
        router: React.PropTypes.object.isRequired,
    },
    componentDidMount:function(){
		this.init(this.props);
    },
	componentWillReceiveProps: function(nextProps) {
		this.init(nextProps);
	},
	init:function(props){
		var new_msgs = Notifications_Ctrl.new.msgs;
		var found_new_count = 0;
		function is_new(n) {
			for(var i = 0; i<new_msgs.length; i++)
			{	
				if(new_msgs[i].id==n.id)
					return true;
			}
			return false;
		}

		if(this.props.more)
			for(var i = 0;  i <props.items.length; i++)
				if(is_new(props.items[i]))
					found_new_count++;

		this.setState({items:props.items, done:1,new_hidden:new_msgs.length-found_new_count});
	},
	itemClick:function(n) {
		var _this =this;

		this.props.itemClicked();
		EventEmitter.dispatch('toolbar_tpm_empty');
		this.context.router.push('/messages/'+n.senderId);
	},
	more:function() {
		this.context.router.push('/inbox');
	},
	render: function  () {
		var _this = this;

		let styles = {
			msg_list:{
				paddingTop:0,
				paddingBottom:0,
				background: '#FFFFFF'
			},
			msg_more: {
				paddingBottom: 10,
	    		paddingTop: 6,
	    		textAlign:'center'
			},
			header:{
			    fontWeight:500
			}
		};

		var get_date = function(s){
			var d = new Date(s);
			return Date_Time.commonDateTimeFormat(d, true);
		};

		var current_header = null;

		var get_header = function(n) {
            var d = new Date(n.dateCreated);
			var time_ago = Date_Time.get_time_ago(d);
			var time_ago_unit = Date_Time.get_time_ago(d,true);
			if(current_header && current_header == time_ago_unit)
				return null;

			current_header = time_ago_unit;

			return (
				<ListItem
			  	secondaryText={time_ago}
			    innerDivStyle={{paddingTop:16}}
			    style={styles.header}
			    disabled={true} />
		    );
		};

		var items = (
			<div>
				{this.state.items.map(function(n, i){

		   			return <div key={i}>
				   			{_this.props.headers ? get_header(n) : null }
		   					<ListItem
					   			leftAvatar={<Avatar src={Config.files_path+n.senderFilesFile} />}
							    primaryText={<div>{n.senderFirstname + ' ' + n.senderLastname}{_this.props.showDate ? <p style={{color:'rgba(0, 0, 0, 0.54)',fontSize:'15px'}}>{decodeURIComponent(n.content)}</p> : null}</div>}
							    secondaryText={_this.props.showDate ? <div style={{textAlign:'right'}}>{get_date(n.dateCreated)}</div> : decodeURIComponent(n.content)}
					    		secondaryTextLines={1}
					    		innerDivStyle={!Number(n.read) ? {background:'#D2D1D1'}:null}
					    		onClick={_this.itemClick.bind(null, n)}
					    		 />
				    		 </div>
				})}
				{this.state.items.length == 0 ? (
					<ListItem disabled={true} secondaryText="No messages" />
				) : null}						    
				{this.props.more && this.state.items.length >= this.props.more ? (
					<ListItem
						secondaryText={this.state.new_hidden>0?(this.state.new_hidden>5?"5+":this.state.new_hidden)+" More Unseen Messages...":"More"}
						innerDivStyle={styles.msg_more}
						onClick={this.more} />
				) : null}
			</div>
			);

		if(!this.state.done)
			return null;
		else
		{
			return <List style={styles.msg_list}>{items}</List>;

		}
	} 
});

module.exports = InboxListItems;