const React = require('react');

import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';

const update = require('react-addons-update');

const Strings_Ctrl = require('../Common/Strings_Ctrl');
const Locations = require('../Common/Locations');
const Notifications_Ctrl = require('../Common/Notifications');
const Date_Time = require('../Common/Date_Time');

var NotificationsListItems = React.createClass({
	getInitialState: function(){
		return {
			items:[],
			done:0,
			new_hidden:0,
		}
	},
	contextTypes: {
        router: React.PropTypes.object.isRequired,
    },
    componentDidMount: function() {
    	this.init(this.props);
    },
	componentWillReceiveProps: function(nextProps) {
		this.init(nextProps);
	},
	init:function(props){
		var notes = props.items;
		var items = [];

		var new_notes = Notifications_Ctrl.new.notes;
		var found_new_count = 0;
		function is_new(n) {
			for(var i = 0; i<new_notes.length; i++)
			{	
				if(new_notes[i].id==n.id)
					return true;
			}
			return false;
		}

		dance:
		for(var i = 0;  i <notes.length; i++)
		{
			var n = notes[i];
			switch(n.type)
			{
				case ENUMS.NOTE_NEW_MSG_SENT:
					break;
				default:
					if(this.props.more && items.length >= this.props.more)
						break dance;
					
					if(this.props.more)
						if(is_new(n))
							found_new_count++;

				 	var item = $.extend({},n);

				 	switch(n.type)
				 	{
				 		case ENUMS.NOTE_OFFER_HOST_MADE:
        		            item.content = i18n.t('js:some1 offer accomm dates', {
        		            	name: n.userFromFirstname+' '+n.userFromLastname,
        		            	date1: Date_Time.commonDateFormat(new Date(n.offerFromDate)), date2: Date_Time.commonDateFormat(new Date(n.offerUntilDate)),
        		            	location: Locations.get_destination({country:n.offerTripCountry, locale:n.offerTripLocale})
        		            });
        		            if(n.offerStatus == ENUMS.OFFER_STATUS_ACCEPTED)
        		            	item.content += ' ' + i18n.t('js:You accepted!');
        		            else if(n.offerStatus == ENUMS.OFFER_STATUS_REJECTED)
        		            	item.content += ' ' + 'You declined.';
        		            item.scrollTo = 'offersR';
							break;
			            case ENUMS.NOTE_OFFER_ACCEPTED:
				            item.content = i18n.t('js:accepted accomm offer dates', {
        		            	name: n.userFromFirstname+' '+n.userFromLastname,
        		            	date1: Date_Time.commonDateFormat(new Date(n.offerFromDate)), date2: Date_Time.commonDateFormat(new Date(n.offerUntilDate)),
        		            	location: Locations.get_destination({country:n.offerTripCountry, locale:n.offerTripLocale})
        		            });
        		            item.scrollTo = 'offersM';
							break;
			            case ENUMS.NOTE_OFFER_REJECTED:
			            	item.content = i18n.t('js:turned down offer dates', {
        		            	name: n.userFromFirstname+' '+n.userFromLastname,
        		            	date1: Date_Time.commonDateFormat(new Date(n.offerFromDate)), date2: Date_Time.commonDateFormat(new Date(n.offerUntilDate)),
        		            	location: Locations.get_destination({country:n.offerTripCountry, locale:n.offerTripLocale})
        		            });
        		            item.scrollTo = 'offersM';
							break;
			            case ENUMS.NOTE_OFFER_TRIP_UPDATED:
				            item.content = i18n.t('js:itinerary changed unchanged', {
        		            	name: n.userFromFirstname+' '+n.userFromLastname,
        		            	date1: Date_Time.commonDateFormat(new Date(n.offerFromDate)), date2: Date_Time.commonDateFormat(new Date(n.offerUntilDate)),
        		            	location: Locations.get_destination({country:n.offerTripCountry, locale:n.offerTripLocale})
        		            });
	                    	break;
            			case ENUMS.NOTE_OFFER_TRIP_CANCELED:
	            			item.content = i18n.t('js:offered trip canceled', {
        		            	name: n.userFromFirstname+' '+n.userFromLastname,
        		            	location: Locations.get_destination({country:n.offerTripCountry, locale:n.offerTripLocale})
        		            });
		                    break;
			            case ENUMS.NOTE_OFFER_HOST_CANCELED:
				            item.content = i18n.t('js:accomm host cancel', {
        		            	name: n.userFromFirstname+' '+n.userFromLastname,
        		            	location: Locations.get_destination({country:n.offerTripCountry, locale:n.offerTripLocale})
        		            });
		                    break;
			            case ENUMS.NOTE_OFFER_CANCEL_ACCEPTED:
			            	item.content = '<strong>'+n.userFromFirstname+' '+n.userFromLastname+'</strong> canceled an accepted accommodation offer for ' +
			            		Locations.get_destination({country:n.offerTripCountry, locale:n.offerTripLocale});
				            break;
	                    case ENUMS.NOTE_FRIEND_NEW_TRIP:
		                    item.content = i18n.t('js:some1 planned trip loc', {
		                    	name: n.userFromFirstname+' '+n.userFromLastname, 
		                    	location: Locations.get_destination({country:n.tripCountry, locale:n.tripLocale})
		                    });
		                    break;
		                case ENUMS.NOTE_FOF_NEW_TRIP:
		                    item.content = i18n.t('js:fof planned trip loc', {
		                    	name: n.userFromFirstname+' '+n.userFromLastname, 
		                    	location: Locations.get_destination({country:n.tripCountry, locale:n.tripLocale})
		                    });
		                    break;
						case ENUMS.NOTE_FRIEND_JOINED:
							item.content = i18n.t('js:some1 joined weestay', {name: n.userFromFirstname+' '+n.userFromLastname});
		                    break;
	                    case ENUMS.NOTE_NETWORK_INVITE_ACCEPTED:
							item.content = i18n.t('js:some1 network invite accept', {name: n.userFromFirstname+' '+n.userFromLastname, networkName:n.networkName});
		                    break;
				 	}
				 	items.push(item);
					break;
			}
		}

		this.setState({items:items,done:1,new_hidden:new_notes.length-found_new_count});
	},
	itemClick:function(n) {
		var _this =this;

		switch(n.type)
		{
			case ENUMS.NOTE_NETWORK_INVITE_ACCEPTED:
			case ENUMS.NOTE_OFFER_TRIP_CANCELED:
				var initially_read = n.read;
				Notifications_Ctrl.read(n, function () {
	                if(!initially_read)
	                	Notifications_Ctrl.Start();
	            });
	            
	            if(n.type==ENUMS.NOTE_NETWORK_INVITE_ACCEPTED)
	            	this.context.router.push('/profile/'+n.userFromId);
	            break;
            case ENUMS.NOTE_FRIEND_JOINED:
            	this.props.itemClicked();
            	if(this.context.router.isActive('/profile/' + n.userFromId))
					EventEmitter.dispatch('toolbar_forceTabChange', 'profile');
				else
					this.context.router.push('/profile/'+n.userFromId);
            	break;
			 case ENUMS.NOTE_OFFER_HOST_MADE:
            case ENUMS.NOTE_OFFER_HOST_UPDATED:
            case ENUMS.NOTE_OFFER_HOST_CANCELED:
            case ENUMS.NOTE_OFFER_ACCEPTED:
            case ENUMS.NOTE_OFFER_REJECTED:
            case ENUMS.NOTE_OFFER_CANCEL_ACCEPTED:
            case ENUMS.NOTE_OFFER_TRIP_UPDATED:
				this.props.itemClicked();
				if(this.context.router.isActive('/trip/' + n.offerTripId))
				{
					EventEmitter.dispatch('toolbar_forceTabChange', 'trip');
					EventEmitter.dispatch('scroll_'+n.scrollTo);
				}
				else
					this.context.router.push('/trip/' + n.offerTripId + (n.scrollTo ? '?'+n.scrollTo+'=1' : ''));
				break;
	        case ENUMS.NOTE_FRIEND_NEW_TRIP:
	        case ENUMS.NOTE_FOF_NEW_TRIP:
		        this.props.itemClicked();
				if(this.context.router.isActive('/trip/' + n.tripId))
					EventEmitter.dispatch('toolbar_forceTabChange', 'trip');
				else
					this.context.router.push('/trip/' + n.tripId);
		        break;
		}
	},
	more:function() {
		this.context.router.push('/notifications');
	},
	render: function  () {
		
		var _this = this;

		let styles = {
			msg_list:{
				paddingTop:0,
				paddingBottom:0,
				background: '#FFFFFF'
			},
			msg_more: {
				paddingBottom: 10,
	    		paddingTop: 6,
	    		textAlign:'center'
			}
		};

		let content = function(txt){
			return (
				<span dangerouslySetInnerHTML={{__html: txt}}/>
			);
		};

		var get_date = function(s){
			var d = new Date(s);
			return Date_Time.commonDateTimeFormat(d, true);
		};

		let items = (
			<div>
				{this.state.items.map(function(n, i){
		   			return <ListItem
			   			key={i} 
			    		leftAvatar={<Avatar src={Config.files_path+n.userFromFilesFile} />}
			    		primaryText={_this.props.showDate ? content(n.content) : null}
			    		secondaryText={_this.props.showDate ? <div style={{textAlign:'right'}}>{get_date(n.dateCreated)}</div> : content(n.content)}
			    		secondaryTextLines={_this.props.showDate ? 1 : 2}
			    		innerDivStyle={Number(n.read) ? null:{background:'#D2D1D1'}}
			    		onClick={_this.itemClick.bind(null, n)}
			    		 />
				})}
				{this.state.items.length == 0 ? (
					<ListItem disabled={true} secondaryText="No notifications" />
				) : null}						    
				{this.props.more && this.state.items.length >= this.props.more ? (
					<ListItem
						secondaryText={this.state.new_hidden>0?(this.state.new_hidden>5?"5+":this.state.new_hidden)+" More Unseen Notifications...":"More"}
						innerDivStyle={styles.msg_more}
						onClick={this.more}
						/>
				) : null}
			</div>
			);

		if(!this.state.done)
			return null;
		else
		{
			return <List style={styles.msg_list}>{items}</List>;
		}
	} 
});

module.exports = NotificationsListItems;