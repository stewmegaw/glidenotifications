<?php

namespace GlideNotifications;

return array(
    'service_manager' => array(
        'factories' => array(//services constructed with arguments
//service factories
            'GlideNotifications\Service\NotificationAccess' => 'GlideNotifications\Service\NotificationAccessServiceFactory',
            'GlideNotifications\Service\NotificationDispatch' => 'GlideNotifications\Service\NotificationDispatchServiceFactory',
            'GlideNotifications\Service\NotificationCRUD' => 'GlideNotifications\Service\NotificationCRUDServiceFactory',
            'GlideNotifications\Service\Email' => 'GlideNotifications\Service\EmailServiceFactory',
            'GlideNotifications\Service\Message' => 'GlideNotifications\Service\MessageServiceFactory',
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'GlideNotifications\Controller\Email' => 'GlideNotifications\Controller\EmailControllerFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'email' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/email/:action[/:param1][/:param2]',
                    'defaults' => array(
                        'controller' => 'GlideNotifications\Controller\Email',
                        'action' => 'generalTest',
                    ),
                ),
            ),
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
                'email_cron_route' => array(
                    'options' => array(
                        'route' => 'general email',
                        'defaults' => array(
                            'controller' => 'GlideNotifications\Controller\Email',
                            'action' => 'general',
                        ),
                    ),
                ),
            )
        )
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
    ),
);
